const pg = require('pg');
const connectionString = process.env.DATABASE_URL || '        postgres://jvvoqzzqdnatgp:d8689bd7d386f223e2f00bb8c7504273ed33b801505572fbd9e7adc143270508@ec2-174-129-192-200.compute-1.amazonaws.com:5432/d2donfb4bgi8dh';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
  'CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
query.on('end', () => { client.end(); });
